﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Test/Shader First"{

	Properties{
		_MainTexture("Main Color (Rgb)", 2D) = "White"{}
		_Color("Color", Color) = (1,1,1,1)
		_DissolveTexture("Cheese",2D) = "white"{}
		_DissolveAmount("Cheese Cut Out Amount", Range(0,1)) = 1
		_ExtrudeAmount("Extrude Amount", float) = 1
		_TopColor("Top Color", Color) = (1,1,1,0.5)
		_BottomColor("Bottom Color", Color) = (1,1,1,0.5)
		_GradientBlendAmount("Gradient Blend Amount", float) = 1

	}

	SubShader{
		Pass{
			CGPROGRAM

			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction
			#include "UnityCG.cginc"
			struct appdata {
				float4 vertex:POSITION;
				float2 uv:TEXCOORD0;
				float3 normal:NORMAL;
			};

		struct v2f {
			float4 position:SV_POSITION;
			float2 uv:TEXCOORD0;
			float3 worldPosition:TEXCOORD1;

		};

		float4 _Color;
		sampler2D _MainTexture;
		float4 _TopColor;
		float4 _BottomColor;
		float _GradientBlendAmount;


		sampler2D _DissolveTexture;
		float _DissolveAmount;

		float _ExtrudeAmount;

			v2f vertexFunction(appdata IN) {
				v2f OUT;
				
				IN.vertex.xyz += IN.normal.xyz  * _ExtrudeAmount* sin(_Time.y);
				OUT.position = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.worldPosition = mul(unity_ObjectToWorld, IN.vertex).xyz;
				OUT.uv = IN.uv;
				return OUT;
			}

			fixed4 fragmentFunction(v2f IN) : SV_Target{
				
				float4 textureColor = tex2D(_MainTexture, IN.uv);
				float4 dissolveColor = tex2D(_DissolveTexture, IN.uv);

				clip(dissolveColor.rgb - _DissolveAmount);
				//float4 gradientColor = lerp(_BottomColor, _TopColor, (IN.worldPosition.y/25)+130 * _GradientBlendAmount);
				float4 gradientColor = _Color*(_BottomColor-_TopColor);

				return  textureColor;
			}


			ENDCG
		}
	}

}