﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeModifyObjectType
{
    [MenuItem("Assets/Create/Object/Object Modification")]
    public static void CreateMyAsset()
    {
        ModifyObjectType asset = ScriptableObject.CreateInstance<ModifyObjectType>();

        AssetDatabase.CreateAsset(asset, "Assets/Scriptableobjects/Object Modifications/New Type.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}