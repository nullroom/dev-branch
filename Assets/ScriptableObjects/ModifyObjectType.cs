﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyObjectType : ScriptableObject
{

    public bool m_KillPlayer;
    public bool screenShake;
    public bool knockBack;
    public ScreenShakeData screenShakeData;
    Player _player;
    Player player
    {
        get { if (_player) { return _player; } else { return _player = Player.instance; } }
    }
    public void Initialize()
    {

        if (m_KillPlayer)
            player.transform.position = player.doorEntered.transform.position;
        else
            Debug.Log("Not Died");

        if (screenShake)
        {
            player.currentScreenShake = screenShakeData;
            if (screenShakeData.normalShake)
                player.GetComponentInChildren<ScreenShake>().normalShake = true;
            if (screenShakeData.projectionShake)
                player.GetComponentInChildren<ScreenShake>().projectionShake = true;
        }
        if (knockBack)
        {
            player.transform.position = player.transform.position - player.transform.forward;
        }

    }
    bool test = false;
    public void FlipActive(GameObject obj)
    {
        obj.SetActive(!obj.activeSelf);
    }
}
