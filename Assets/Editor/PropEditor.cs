﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Prop))]
[CanEditMultipleObjects]
public class PropEditor : Editor
{
    bool displayWarning;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Prop prop = (Prop)target;
        if (PrefabUtility.GetPrefabParent(prop.gameObject) == null)
        {
            EditorGUILayout.HelpBox("This object is not a prefeb!", MessageType.Error);
        }
        if (GUILayout.Button("Create Prefab"))
        {
            Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/Resources/Modules/" + prop.gameObject.name + ".prefab");
            PrefabUtility.ReplacePrefab(prop.gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
        }
    }
}