﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public float aggressionRadius;
    public float attackRadius;
    public Transform hip;

    Animator anim;

    float lerpTime = 1f;
    float currentLerpTime;
    Vector3 startPos;
    Vector3 endPos;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        hip = transform.FindChild("Hips");
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        if (Vector3.Distance(hip.position, Player.instance.gameObject.transform.position) < attackRadius)
        {
            if (anim.GetBool("Move") == true)
                anim.SetBool("Move", false);
            if (anim.GetBool("Move") == false)
                anim.SetTrigger("Attack");
        }
        else if (Vector3.Distance(hip.position, Player.instance.gameObject.transform.position) < aggressionRadius)
        {
            if (anim.GetBool("Move") == false)
            {
                currentLerpTime = 0;
                anim.SetBool("Move", true);
            }
            MoveToo(Player.instance.gameObject);
        }else
        {
            anim.SetBool("Move", false);
        }
    }
    void MoveToo(GameObject obj)
    {
        var lookDir = Player.instance.transform.position - transform.position;
        lookDir.y = 0; // keep only the horizontal direction
        transform.rotation = Quaternion.LookRotation(lookDir);
        startPos = transform.position;
        endPos = new Vector3(Player.instance.transform.position.x, transform.position.y, Player.instance.transform.position.z);

        currentLerpTime += Time.deltaTime * 0.005f;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = lerpTime;
        }
        float perc = currentLerpTime / lerpTime;
        transform.position = Vector3.Lerp(startPos, endPos, perc);
    }
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(hip.position, aggressionRadius);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(hip.position, attackRadius);

    }
#endif
}
