﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ModuleGenerator : MonoBehaviour
{
    public Module[] _availableRooms;
    public List<Module> _existingRooms = new List<Module>();
    public Player p_Player;
    public Player player;

    public static ModuleGenerator instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        _availableRooms = Resources.LoadAll("Modules", typeof(Module)).Cast<Module>().ToArray();
        //PickFirstRoom();
        player = Instantiate(p_Player);
        var respawn = GameObject.FindGameObjectWithTag("Respawn");
        player.transform.position = respawn.transform.position;
    }

    private void PickFirstRoom()
    {
        var starterModels = _availableRooms.Where(m => m.GetType() == typeof(Room)).ToArray();
        var _startModule = starterModels[Random.Range(0, starterModels.Length)].GetComponent<Module>();
        var clone = _startModule;
        var firstRoom = Instantiate(clone);
        firstRoom.transform.position = transform.position;
        _existingRooms.Add(firstRoom.GetComponent<Module>());
    }
    // Update is called once per frame
    void Update()
    {
            
    }
}
