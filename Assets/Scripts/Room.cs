﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Room : Module
{
    // Use this for initialization
    public Module[] availableRooms = new Module[] { };
    public ModuleConnector[] doors = new ModuleConnector[] { };
    void Start()
    {
        var rooms = ModuleGenerator.instance._existingRooms;
        for (var i = 0; i < rooms.Count; i++)
        {
            if (rooms[i].gameObject.GetInstanceID() != gameObject.GetInstanceID())
            {
                if (rooms[i].GetType() == typeof(Room))
                {
                    if (GetComponent<Collider>().bounds.Intersects(rooms[i].GetComponent<Collider>().bounds))
                    {
                        if (connectorSpawnedOn)
                        {
                            if(connectorSpawnedOn.module && connectorSpawnedOn.module.GetComponentInChildren<Animator>())
                            connectorSpawnedOn.module.GetComponentInChildren<Animator>().SetBool("open", false);
                        }
                        ModuleGenerator.instance._existingRooms.Remove(this);
                        connectorSpawnedOn.connectedRoom.module.transform.FindChild("doorOC_002").FindChild("wall").gameObject.SetActive(true);
                        connectorSpawnedOn.connectedRoom.module.transform.FindChild("doorOC_002").FindChild("door_GEO").gameObject.SetActive(false);
                        connectorSpawnedOn.hasConnected = false;
                        connectorSpawnedOn.connectedRoom.hasConnected = false;
                        Destroy(this.gameObject);
                    }
                }
            }
        }
        GameObject.FindObjectOfType<ProceduralGridMover>().ResetGraph();
    }
    public void CloseAllDoors()
    {
        var exits = GetExits;
        for (var i = 0; i < doors.Length; i++)
        {
            doors[i].connectedRoom.GetComponentInParent<Door>().GetComponentInChildren<Animator>().SetBool("open", false);
            doors[i].connectedRoom.OppositeConnector.hasConnected = false;
           // doors[i].connectedRoom.hasConnected = false;
        }
    }
    public ModuleConnector[] GetLights
    {
        get
        {
            var exits = GetComponentsInChildren<ModuleConnector>();
            var listToReturn = new List<ModuleConnector>();
            for (var i = 0; i < exits.Length; i++)
            {
                if (exits[i].ContainsTag("Light") || exits[i].ContainsTag("light"))
                {
                    listToReturn.Add(exits[i]);
                }
            }
            return listToReturn.ToArray();
        }
    }
    bool ran = false;
    private void Update()
    {
        if(availableRooms.Length == 0)
        {
            availableRooms = ModuleGenerator.instance._availableRooms;
        }
        if(doors.Length == 0)
            doors = GetExits.Where(x => x && x.connectedRoom && x.connectedRoom.GetComponentInParent<Door>()).ToArray();
        if (availableRooms.Length > 0 && ran == false)
        {
            for (var i = 0; i < GetExits.Length; i++)
            {
                if (!GetExits[i].connectedRoom)
                    GenerateNewRoom(GetExits[i]);
                else
                    connectorSpawnedOn = GetExits[i];
            }
            ran = true;
        }
        for (var i = 0; i < doors.Length; i++)
        {
            if(doors[i].connectedRoom.OppositeConnector && doors[i].connectedRoom.OppositeConnector.connectedRoom && doors[i].connectedRoom.GetComponentInParent<Door>().GetComponentInChildren<Animator>().GetBool("open") == false && doors[i].connectedRoom.GetComponentInParent<Door>().GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).fullPathHash == 743071042)
            {

                doors[i].connectedRoom.GetComponentInParent<Door>().GetComponentInChildren<Animator>().SetBool("open", true);
            }
        }
        ShouldDestroy();
    }
    bool destroyd = false;
    void ShouldDestroy()
    {

        if (!Player.instance.currentRoom)
            return;
        if (this.gameObject.GetInstanceID() != Player.instance.currentRoom.gameObject.GetInstanceID())
        {
            var exits = GetExits;
            for (var i = 0; i < exits.Length; i++)
            {
                if (exits[i].connectedRoom && exits[i].connectedRoom.module.gameObject.GetInstanceID() == Player.instance.currentRoom.gameObject.GetInstanceID())
                {
                    return;
                }
                if (exits[i].connectedRoom == null && exits[i].hasConnected)
                {
                    destroyd = true;
                }
            }
            if (destroyd)
            {
                ModuleGenerator.instance._existingRooms.Remove(this);
                print("Destroyed " + gameObject.name);
                Destroy(gameObject);
                destroyd = false;
            }
        }
    }
}
