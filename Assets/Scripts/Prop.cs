﻿using UnityEngine;
using System.Collections;

public class Prop : Module
{
    Color[] colors = new Color[] { new Color(0.5f,0,0), Color.black, new Color(0, 0, 0.5f), new Color(0, 0.5f, 0.5f), Color.gray, new Color(0f, 0.212f, 0f), new Color(0.5f, 0, 0.5f), new Color(0.5f, 0.6f, 0.1f) };
    void Start()
    {

        for (var i = 0; i < GetExits.Length; i++)
        {
            if (!GetExits[i].connectedRoom)
                GenerateNewRoom(GetExits[i]);
            else
                connectorSpawnedOn = GetExits[i];
        }
        if (Tags[0] == "TubeLight")
        {
            GetComponentInChildren<TubeLight>().m_Intensity = connectorSpawnedOn.connectedRoom.m_Intensity;
            if (connectorSpawnedOn.connectedRoom.randomLightcolor)
            {
                GetComponentInChildren<TubeLight>().m_Color = colors[Random.Range((int)0, (int)colors.Length - 1)];

            }
            else
                GetComponentInChildren<TubeLight>().m_Color = connectorSpawnedOn.connectedRoom.m_Color;
            GetComponentInChildren<TubeLight>().m_Range = connectorSpawnedOn.connectedRoom.m_Range;
            GetComponentInChildren<TubeLight>().m_Radius = connectorSpawnedOn.connectedRoom.m_Radius;
            GetComponentInChildren<TubeLight>().m_Length = connectorSpawnedOn.connectedRoom.m_Length;
        }

    }
}
