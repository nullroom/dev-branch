﻿using UnityEngine;
using System.Collections;
using System;

// based on http://unitytipsandtricks.blogspot.com/2013/05/camera-shake.html
[Serializable]
public class ScreenShakeData
{//set up this class on player then import data from scriptable objects 
    [SerializeField]
    public bool normalShake = false;
    [SerializeField]
    public bool projectionShake = false;
    [SerializeField]
    public float duration = 2f;
    [SerializeField]
    public float speed = 20f;
    [SerializeField]
    public float magnitude = 2f;
    [SerializeField]
    public AnimationCurve damper = new AnimationCurve(new Keyframe(0f, 1f), new Keyframe(0.9f, .33f, -2f, -2f), new Keyframe(1f, 0f, -5.65f, -5.65f));
}

public class ScreenShake : MonoBehaviour
{

    [SerializeField]
    public bool normalShake = false;
    [SerializeField]
    public bool projectionShake = false;
    Vector3 originalPos;

    public Player player;
    ScreenShakeData screenShakeData
    {
        get { if (!player) { return null; }  return player.currentScreenShake; }
    }
    void OnEnable()
    {
        originalPos = transform.localPosition;
    }

    private void Start()
    {
        player = Player.instance;

    }

    void Update()
    {
        if (screenShakeData == null)
            return;
        if (normalShake)
        {
            normalShake = false;
            StopAllCoroutines();
            StartCoroutine(Shake(transform, originalPos, screenShakeData.duration, screenShakeData.speed, screenShakeData.magnitude, screenShakeData.damper));
        }
        else if (projectionShake)
        {
            projectionShake = false;
            StopAllCoroutines();
            StartCoroutine(ShakeCamera(Camera.main, screenShakeData.duration, screenShakeData.speed, screenShakeData.magnitude / 10, screenShakeData.damper));
        }
    }

    public void InitializeScreenShake()
    {
        StopAllCoroutines();
        StartCoroutine(ShakeCamera(Camera.main, screenShakeData.duration, screenShakeData.speed, screenShakeData.magnitude / 10, screenShakeData.damper));
    }
    public void InitializeScreenShake(float dur, float s, float m)
    {
        StopAllCoroutines();
        StartCoroutine(ShakeCamera(Camera.main, screenShakeData.duration + dur, screenShakeData.speed + s, (screenShakeData.magnitude + m) / 10, screenShakeData.damper));
    }
    IEnumerator Shake(Transform transform, Vector3 originalPosition, float duration, float speed, float magnitude, AnimationCurve damper = null)
    {
        float elapsed = 0f;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float damperedMag = (damper != null) ? (damper.Evaluate(elapsed / duration) * magnitude) : magnitude;
            float x = (Mathf.PerlinNoise(Time.time * speed, 0f) * damperedMag) - (damperedMag / 2f);
            float y = (Mathf.PerlinNoise(0f, Time.time * speed) * damperedMag) - (damperedMag / 2f);
            transform.localPosition = new Vector3(originalPosition.x + x, originalPosition.y + y, originalPosition.z);
            yield return null;
        }
        transform.localPosition = originalPosition;
    }


    IEnumerator ShakeCamera(Camera camera, float duration, float speed, float magnitude, AnimationCurve damper = null)
    {
        float elapsed = 0f;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float damperedMag = (damper != null) ? (damper.Evaluate(elapsed / duration) * magnitude) : magnitude;
            float x = (Mathf.PerlinNoise(Time.time * speed, 0f) * damperedMag) - (damperedMag / 2f);
            float y = (Mathf.PerlinNoise(0f, Time.time * speed) * damperedMag) - (damperedMag / 2f);
            // offset camera obliqueness - http://answers.unity3d.com/questions/774164/is-it-possible-to-shake-the-screen-rather-than-sha.html
            float frustrumHeight = 2 * camera.nearClipPlane * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
            float frustrumWidth = frustrumHeight * camera.aspect;
            Matrix4x4 mat = camera.projectionMatrix;
            mat[0, 2] = 2 * x / frustrumWidth;
            mat[1, 2] = 2 * y / frustrumHeight;
            camera.projectionMatrix = mat;
            yield return null;
        }
        camera.ResetProjectionMatrix();
    }

}