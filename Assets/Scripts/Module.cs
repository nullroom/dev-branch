﻿using UnityEngine;
using System.Linq;
using UnityEditor;

public abstract class Module : MonoBehaviour
{
    protected bool spawnNew;
    public ModuleConnector connectorSpawnedOn;
    [Tooltip("The name of this module")]
    public string[] Tags;
    public ModuleConnector[] GetExits
    {
        get { if (_childConnectors.Length == 0) { return _childConnectors = GetComponentsInChildren<ModuleConnector>(); } else return _childConnectors;  }
    }
    ModuleConnector[] _childConnectors = new ModuleConnector[] { };
    public ModuleConnector[] GetExitsWithTag(string tagToMatch)
    {
        var exits = GetComponentsInChildren<ModuleConnector>();
        return exits.Where(e => e.ContainsTag(tagToMatch)).ToArray();
    }
    bool destroyz = false;
    void Update()
    {
        if (!Player.instance.currentRoom)
            return;
        if (this.gameObject.GetInstanceID() != Player.instance.currentRoom.gameObject.GetInstanceID())
        {
            var exits = GetExits;
            for (var i = 0; i < exits.Length; i++)
            {
                if (exits[i].connectedRoom && exits[i].connectedRoom.module.gameObject.GetInstanceID() == Player.instance.currentRoom.gameObject.GetInstanceID())
                {
                    return;
                }
                if (exits[i].connectedRoom == null && exits[i].hasConnected)
                {
                    destroyz = true;
                }
            }
            if (destroyz)
            {
                ModuleGenerator.instance._existingRooms.Remove(this);
                Destroy(gameObject);
                destroyz = false;
            }
        }

    }
    public void GenerateNewRoom(ModuleConnector connectorToSpawnOn)
    {
        var newTag = Misc.GetRandom(connectorToSpawnOn.Tags);
        var newModulePrefab = Misc.GetRandomWithTag(ModuleGenerator.instance._availableRooms, newTag);
        var newModule = (Module)Instantiate(newModulePrefab);
        var newModuleExits = newModule.GetExits;
        //var exitToMatch = newModuleExits.FirstOrDefault(x => x.IsDefault) ?? Misc.GetRandom(newModuleExits);
        ModuleConnector exitToMatch;
        var exitsToMatch = newModuleExits.Where(x => x.IsDefault).ToArray();
        //print(exitsToMatch.Length);
        if (exitsToMatch.Length > 0)
            exitToMatch = Misc.GetRandom(exitsToMatch);
        else
            exitToMatch = Misc.GetRandom(newModuleExits);
        MatchExits(connectorToSpawnOn, exitToMatch);
        ModuleGenerator.instance._existingRooms.Add(newModule);
        connectorToSpawnOn.connectedRoom = exitToMatch;
        exitToMatch.connectedRoom = connectorToSpawnOn;
        connectorToSpawnOn.hasConnected = true;
        exitToMatch.hasConnected = true;
        //newExits.AddRange(newModuleExits.Where(e => e != exitToMatch));
    }
   
    private void MatchExits(ModuleConnector oldExit, ModuleConnector newExit)
    {//
        var newModule = newExit.transform.parent;
        var forwardVectorToMatch = -oldExit.transform.forward;
        var correctiveRotation = Misc.Azimuth(forwardVectorToMatch) - Misc.Azimuth(newExit.transform.forward);
        newModule.RotateAround(newExit.transform.position, Vector3.up, correctiveRotation);
        var correctiveTranslation = oldExit.transform.position - newExit.transform.position;
        newModule.transform.position += correctiveTranslation;
    }
    void OnDestroy()
    {
        ModuleGenerator.instance._existingRooms.Remove(this);
        if (spawnNew)
        {
            GenerateNewRoom(connectorSpawnedOn);
        }

    }
}
